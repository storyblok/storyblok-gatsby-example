module.exports = {
  siteMetadata: {
    title: 'Gatsby Default Starter',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-source-storyblok',
      options: {
        accessToken: 'RdqDe1WHDvFXIwKNLFrQsQtt',
        homeSlug: 'home',
        version: 'draft'
      }
    }
  ]
};
