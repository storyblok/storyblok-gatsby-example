import React from 'react'
import Link from 'gatsby-link'

class BlogIndex extends React.Component {
  render() {
    const entries = this.props.data.allStoryblokEntry

    return (
      <div className="container">
        <div className="row">
          {entries.edges.map((edge) => {
            edge.node.content = JSON.parse(edge.node.content)
            return <div className="col-md-6 mb-4" key={edge.node.id}>
              <Link className="card" to={'/' + edge.node.full_slug}>
                <img className="card-img-top" src={edge.node.content.image}/>
                <div className="card-body">
                  <h3 className="card-title">{edge.node.content.title}</h3>
                </div>
              </Link>
            </div>})}
        </div>
      </div>
    )
  }
}

export default BlogIndex

export const pageQuery = graphql`
query postOverview {
  allStoryblokEntry {
    edges {
      node {
        id
        full_slug
        content
      }
    }
  }
}
`;
